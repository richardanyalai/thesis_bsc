# Szakdolgozat

## Bevezetés

Ezen git repository tartalmazza a Szegedi Tudományegyetem Természettudományi és
Informatikai Kar Programtervező Informatikus hallgatójaként elkészített
szakdolgozatomat. A szakdolgozathoz írt projekt megtalálható
[itt](https://gitlab.com/richardanyalai/rexos).

A szakdolgozatomat LaTeX-ben írtam, és fordításához a _xelatex_ programot
használtam. A dokumentumban használt betűtípus a _Nimbus Roman_, ami a
_Times New Roman_ font Free and Open Source változata (a GNU General Public
Licence alá tartozik).

## Használati utasítások

### Szükséges csomagok

- texlive-bin
- texlive-core
- texlive-fontsextra
- texlive-latexextra
- texlive-pictures
- texlive-bibtexextra

```bash
# dokumentum kompilálása
make

# dokumentum megnyitása az alapértelmezett PDF-olvasóban
make run

# kompilálás közben keletkezett fájlok törlése
make clean
```

### Hivatkozások

[eredeti sablon](https://github.com/szledan/thesis-template)
